 # InBank Homework
This is the application to process file Test_task_1.csv 

## Introduction

Process file Test_task_1.csv using a script, in any programing language. Follow coding
industry standards.

### Formating rules:
1) Split the file into minimum number of new files so each file had only uniq id-s in
it.
2) All files must have a header.
3) All commas must be replaced with a dot. Example: 4,5 -> 4.5 .
4) Column Date must be formated to dd-mmm-yy. Example: 21/08/2019 -> 21-Aug-
19
5) Column No skip values that are in format 20180615 , values in format
dd/mm/yyyy must be stored if format yyyy-mm-dd. Example: 20180615 ->
20180615 , 15/06/2018 -> 2018-06-15 .

## Installation

```bash
pipenv install
```
the install will take care of installing the Pandas package
[PANDAS](https://pandas.pydata.org/)

```python
pipenv run python main.py
```

## Result
2 csv files are created
