import csv
import sys
import pandas as pd
from datetime import datetime

FILE = f"{sys.path[0]}/Test_task_1.csv"
NO_DATE_FORMAT = '%Y-%m-%d'


def format_timedate(date):
    if '-' in date:
        return str(datetime.strptime(date, '%d-%m-%Y').strftime(NO_DATE_FORMAT))
    elif '/' in date:
        return str(datetime.strptime(date, '%d/%m/%Y').strftime(NO_DATE_FORMAT))
    else:
        return date


def main():
    df = pd.read_csv(FILE, sep=';', parse_dates=['Date'])
    df.sort_values("person_name", inplace=True)

    df['Date'] = df['Date'].dt.strftime('%d-%b-%Y')
    df['No'] = df['No'].apply(
        lambda row: format_timedate(row))
    col = ['Total', 'Paid']
    df[col] = df[col].apply(
        lambda row: row.str.replace(".", "").str.replace(",", "."))

    df_dedup = df["id"].duplicated()

    df.head()
    df[df_dedup].to_csv(f"{sys.path[0]}/test1.csv", index=False)

    ne_stacked = df.drop(df.index[df_dedup])
    ne_stacked.to_csv(f"{sys.path[0]}/test2.csv", index=False)


if __name__ == "__main__":
    main()
